﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpeLuncher_v3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel viewModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            viewModel = new MainWindowViewModel();
            //  S1.IsEnabled = false;
       //     S1.IsEnabled = false;
            

            if (Properties.Settings.Default.ILE_PRZYCISKOW == 1)
            {
                S1.IsEnabled = true;
                S1.Content = Properties.Settings.Default.BUTTON1_NAZWA_PRZYCISKU;
                S2.IsEnabled = false ;
                S3.IsEnabled = false;
                S4.IsEnabled = false;
                S5.IsEnabled = false;
            }
          else  if (Properties.Settings.Default.ILE_PRZYCISKOW == 2)
            {
                S1.IsEnabled = true;
                S1.Content = Properties.Settings.Default.BUTTON1_NAZWA_PRZYCISKU;
                S2.IsEnabled = true;
                S2.Content = Properties.Settings.Default.BUTTON2_NAZWA_PRZYCISKU;
                S3.IsEnabled = false;
                S4.IsEnabled = false;
                S5.IsEnabled = false;
            }
      else      if (Properties.Settings.Default.ILE_PRZYCISKOW == 3)
            {
                S1.IsEnabled = true;
                S1.Content = Properties.Settings.Default.BUTTON1_NAZWA_PRZYCISKU;
                S2.IsEnabled = true;
                S2.Content = Properties.Settings.Default.BUTTON2_NAZWA_PRZYCISKU;
                S3.IsEnabled = true;
                S3.Content = Properties.Settings.Default.BUTTON3_NAZWA_PRZYCISKU;
                S4.IsEnabled = false;
                S5.IsEnabled = false;
            }
       else     if (Properties.Settings.Default.ILE_PRZYCISKOW == 4)
            {
                 S1.IsEnabled = true;
                S1.Content = Properties.Settings.Default.BUTTON1_NAZWA_PRZYCISKU;
                S2.IsEnabled = true;
                S2.Content = Properties.Settings.Default.BUTTON2_NAZWA_PRZYCISKU;
                S3.IsEnabled = true;
                S3.Content = Properties.Settings.Default.BUTTON3_NAZWA_PRZYCISKU;
                S4.IsEnabled = true;
                S4.Content = Properties.Settings.Default.BUTTON4_NAZWA_PRZYCISKU;

                S5.IsEnabled = false;
            }
       else     if (Properties.Settings.Default.ILE_PRZYCISKOW == 5)
            {
                S1.IsEnabled = true;
                S1.Content = Properties.Settings.Default.BUTTON1_NAZWA_PRZYCISKU;
                S2.IsEnabled = true;
                S2.Content = Properties.Settings.Default.BUTTON2_NAZWA_PRZYCISKU;
                S3.IsEnabled = true;
                S3.Content = Properties.Settings.Default.BUTTON3_NAZWA_PRZYCISKU;
                S4.IsEnabled = true;
                S4.Content = Properties.Settings.Default.BUTTON4_NAZWA_PRZYCISKU;
                S5.IsEnabled = true;
                S5.Content = Properties.Settings.Default.BUTTON5_NAZWA_PRZYCISKU;
            }
            else
            {
                MessageBox.Show("Parametr ilosc serwerow poza zakresem");
                this.Close();
            }

        }

        private static bool AddHaspKey(string Hasp)
        {

           
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\CDN\\HASP", true); //otworzenie klucza, w którym obecne są aplikacje uruchamiane z systemem

            //sprawdzenie czy aplikacja znajduje się już w autostarcie

            {
                try
                {

                    // rkApp.SetValue("KonfigConnectStr", "NET:" + konfig + "," + server + ",NT=1"); //dodanie wartości do klucza
                    // rkApp.SetValue("Operator", ope);
                    rkApp.SetValue("Serwer", Hasp);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }

            return false;
        }

        private static bool AddKey(string ServerName, string Operator, string konfigbaza)
        {
            
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\CDN\\CDN Opt!ma\\CDN Opt!ma\\Login\\", true); //otworzenie klucza, w którym obecne są aplikacje uruchamiane z systemem

            //sprawdzenie czy aplikacja znajduje się już w autostarcie

            {
                try
                {

                    rkApp.SetValue("KonfigConnectStr", "NET:" + konfigbaza + "," + ServerName + ",NT=1"); //dodanie wartości do klucza
                    rkApp.SetValue("Operator", Operator);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }

            return false;
        }

        public static bool RemoveHasp()
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\CDN\\HASP", true);


            {
                try
                {
                    rkApp.DeleteValue("Serwer", false); //usunięcie wartości klucza
                                                        //  rkApp.DeleteValue("Operator", false);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;

            }
            return false;
        }
        public static bool RemoveKonfigKey()
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\CDN\\CDN Opt!ma\\CDN Opt!ma\\Login\\", true);


            {
                try
                {
                    rkApp.DeleteValue("KonfigConnectStr", false); //usunięcie wartości klucza
                    rkApp.DeleteValue("Operator", false);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;

            }
            return false;
        }

        private void S1_Click(object sender, RoutedEventArgs e)
        {
            string server = Properties.Settings.Default.BUTTON1_SERWER;
            string ope = Properties.Settings.Default.BUTTON1_OPERATOR;
            string konfig = Properties.Settings.Default.BUTTON1_BAZA_KONF;
            string hasp = Properties.Settings.Default.BUTTON1_HASP;
            RemoveKonfigKey();
            RemoveHasp();
            AddHaspKey(hasp);
            AddKey(server, ope, konfig);
            Process.Start(Properties.Settings.Default.OPTIMA_SCIEZKA);

        }

        private void S2_Click(object sender, RoutedEventArgs e)
        {
            string server = Properties.Settings.Default.BUTTON2_SERWER;
            string ope = Properties.Settings.Default.BUTTON2_OPERATOR;
            string konfig = Properties.Settings.Default.BUTTON2_BAZA_KONF;
            string hasp = Properties.Settings.Default.BUTTON2_HASP;
            RemoveKonfigKey();
            RemoveHasp();
            AddHaspKey(hasp);
            AddKey(server, ope, konfig);
            Process.Start(Properties.Settings.Default.OPTIMA_SCIEZKA);
        }

        private void S3_Click(object sender, RoutedEventArgs e)
        {
            string server = Properties.Settings.Default.BUTTON3_SERWER;
            string ope = Properties.Settings.Default.BUTTON3_OPERATOR;
            string konfig = Properties.Settings.Default.BUTTON3_BAZA_KONF;
            string hasp = Properties.Settings.Default.BUTTON3_HASP;
            RemoveKonfigKey();
            RemoveHasp();
            AddHaspKey(hasp);
            AddKey(server, ope, konfig);
            Process.Start(Properties.Settings.Default.OPTIMA_SCIEZKA);
        }

        private void S4_Click(object sender, RoutedEventArgs e)
        {
            string server = Properties.Settings.Default.BUTTON4_SERWER;
            string ope = Properties.Settings.Default.BUTTON4_OPERATOR;
            string konfig = Properties.Settings.Default.BUTTON4_BAZA_KONF;
            string hasp = Properties.Settings.Default.BUTTON4_HASP;
            RemoveKonfigKey();
            RemoveHasp();
            AddHaspKey(hasp);
            AddKey(server, ope, konfig);
            Process.Start(Properties.Settings.Default.OPTIMA_SCIEZKA);
        }

        private void S5_Click(object sender, RoutedEventArgs e)
        {
            string server = Properties.Settings.Default.BUTTON5_SERWER;
            string ope = Properties.Settings.Default.BUTTON5_OPERATOR;
            string konfig = Properties.Settings.Default.BUTTON5_BAZA_KONF;
            string hasp = Properties.Settings.Default.BUTTON5_HASP;
            RemoveKonfigKey();
            RemoveHasp();
            AddHaspKey(hasp);
            AddKey(server, ope, konfig);
            Process.Start(Properties.Settings.Default.OPTIMA_SCIEZKA);
        }
    }
}
